#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

Mat colors(Mat img)
{
    Mat b = img.clone() & cv::Scalar(255, 0, 0);
    Mat g = img.clone() & cv::Scalar(0, 255, 0);
    Mat r = img.clone() & cv::Scalar(0, 0, 255);

    Mat res;
    hconcat(b, g, res);
    hconcat(res, r, res);

    return res;
}

int main()
{
    Mat img = imread("cubes.png");
    
    vector<int> quality_params = vector<int>(2);

    quality_params[0] = CV_IMWRITE_JPEG_QUALITY;

    quality_params[1] = 65;
    imwrite("jpg65_cubes.jpg", img, quality_params);
    Mat jpg65 = imread("jpg65_cubes.jpg");

    quality_params[1] = 95;
    imwrite("jpg95_cubes.jpg", img, quality_params);
    Mat jpg95 = imread("jpg95_cubes.jpg");

    Mat res65 = colors(img-jpg65)*15;
    Mat res95 = colors(img-jpg95)*15;

    Mat img_gray, jpg65_gray, jpg95_gray;
    cvtColor(img, img_gray, CV_BGR2GRAY);
    cvtColor(jpg65, jpg65_gray, CV_BGR2GRAY);
    cvtColor(jpg95, jpg95_gray, CV_BGR2GRAY);
    Mat res65_gray = (img_gray - jpg65_gray) * 15;
    Mat res95_gray = (img_gray - jpg95_gray) * 15;

    Mat res;
    hconcat(img, jpg65, res);
    hconcat(res, jpg95, res);

    hconcat(img_gray, res65_gray, res65_gray);
    hconcat(res65_gray, res95_gray, res65_gray);
    cvtColor(res65_gray, res65_gray, COLOR_GRAY2RGB);

    vconcat(res, res65, res);
    vconcat(res, res95, res);
    vconcat(res, res65_gray, res);
    imshow("res", res);
    //imwrite("result.png", res);
   

    waitKey(0);
}