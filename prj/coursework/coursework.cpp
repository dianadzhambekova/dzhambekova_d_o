#include <opencv2/opencv.hpp>
#include <iostream>
#include <opencv2/xfeatures2d.hpp>
#include <cmath>

using namespace std;
using namespace cv;
using namespace videostab;

struct TransformParam
{
    double dx;
    double dy;
    double da;
    TransformParam() {}
    TransformParam(double _dx, double _dy, double _da)
    {
        dx = _dx;
        dy = _dy;
        da = _da;
    }

    void getTransform(Mat &T)
    {
        T.at<double>(0, 0) = cos(da);
        T.at<double>(0, 1) = -sin(da);
        T.at<double>(1, 0) = sin(da);
        T.at<double>(1, 1) = cos(da);

        T.at<double>(0, 2) = dx;
        T.at<double>(1, 2) = dy;
    }
};

vector<TransformParam> countTrajectory(vector<TransformParam> &transforms)
{
    vector <TransformParam> trajectory;
    double a = 0;
    double x = 0;
    double y = 0;

    for (size_t i = 0; i < transforms.size(); i++)
    {
        x += transforms[i].dx;
        y += transforms[i].dy;
        a += transforms[i].da;

        trajectory.push_back(TransformParam(x, y, a));
    }

    return trajectory;
}

vector <TransformParam> smoothTrajectory(vector <TransformParam>& trajectory, int radius)
{
    vector <TransformParam> smoothed_trajectory;
    for (size_t i = 0; i < trajectory.size(); i++)
    {
        double sum_x = 0;
        double sum_y = 0;
        double sum_a = 0;
        int count = 0;

        for (int j = -radius; j <= radius; j++)
        {
            if (i + j >= 0 && i + j < trajectory.size())
            {
                sum_x += trajectory[i + j].dx;
                sum_y += trajectory[i + j].dy;
                sum_a += trajectory[i + j].da;

                count++;
            }
        }

        double avg_a = sum_a / count;
        double avg_x = sum_x / count;
        double avg_y = sum_y / count;

        smoothed_trajectory.push_back(TransformParam(avg_x, avg_y, avg_a));
    }

    return smoothed_trajectory;
}

vector<float> countPSNR(string file, int n_frames)
{
    Mat curr, curr_gray;
    Mat prev, prev_gray;
    VideoCapture cap(file);
    //int n_frames = int(cap.get(CAP_PROP_FRAME_COUNT));
    vector<float> PSNR;

    cap.read(prev);
    cvtColor(prev, prev_gray, COLOR_BGR2GRAY);
    Mat f1, f2;
    for (int i = 1; i < n_frames - 1; i += 1)
    {
        Mat diff = prev_gray;
        diff.convertTo(f1, CV_32F, 1.0 / 255.0);

        cap.read(curr);
        if (curr.empty()) break;

        cvtColor(curr, curr_gray, COLOR_BGR2GRAY);
        curr_gray.convertTo(f2, CV_32F, 1.0 / 255.0);
        diff = f2 - f1;

        Mat sq_diff = diff.mul(diff);
        float sum = 0;

        for (int row = 0; row < sq_diff.size().height - 1; row += 1)
        {
            for (int col = 0; col < sq_diff.size().width - 1; col += 1)
            {
                sum += sq_diff.at<float>(row, col);
            }
        }
        sum /= prev.size().height;
        sum /= prev.size().width;

        PSNR.push_back(10 * log(255 * 255 / sum));
    }
    return PSNR;
}

float countSI(vector<TransformParam> traj, float m)
{
    int N = traj.size();
    float SI = 0;
    for (int i = 1; i < N; i += 1)
    {
        SI += m * std::sqrt(std::pow(traj[i].dx - traj[i - 1].dx, 2) + std::pow(traj[i].dy - traj[i - 1].dy, 2));
    }
    SI /= N - 1;
    return SI;
}

int main()
{
    VideoCapture cap("bev_6.avi");

    int n_frames = int(cap.get(CAP_PROP_FRAME_COUNT));
    int w = int(cap.get(CAP_PROP_FRAME_WIDTH));
    int h = int(cap.get(CAP_PROP_FRAME_HEIGHT));
    double fps = cap.get(CV_CAP_PROP_FPS);

    Mat curr, curr_gray;
    Mat prev, prev_gray;

    vector<float> PSNR1 = countPSNR("bev_6.avi", n_frames);
    //for (int i = 0; i < PSNR1.size(); i += 1)
    //{
    //    cout << PSNR1[i] << " ";
    //}

    cap.read(prev);    //��������� 1-� ����
    cvtColor(prev, prev_gray, COLOR_BGR2GRAY);
    
    VideoWriter out("stabilized_bev_6.avi", CV_FOURCC('M', 'J', 'P', 'G'), fps, prev_gray.size());
    VideoWriter of_out("optflow_bev_6.avi", CV_FOURCC('M', 'J', 'P', 'G'), fps, prev_gray.size());

    vector <TransformParam> transforms;

    int num_of_keypoints = 100;
    Ptr<Feature2D> f2d = ORB::create(num_of_keypoints); // ������� ��������
    vector<KeyPoint> keypoints1, keypoints2;
    const  vector< int > & 	keypointIndexes = vector< int >();
    const  vector< int > & 	keypointIndexes2 = vector< int >();

    Mat imgLines = Mat::zeros(prev_gray.size(), CV_8UC3); // ��� ��������� ���. ������

    Mat last_T;

    

    for (int i = 1; i < n_frames - 1; i += 1)
    {
        vector <Point2f> prev_pts, curr_pts;
        vector <uchar> status;
        vector <float> err;

        f2d->detect(prev_gray, keypoints1);
        KeyPoint::convert(keypoints1, prev_pts, keypointIndexes);
        //goodFeaturesToTrack(prev_gray, prev_pts, 200, 0.01, 30);

        cap.read(curr);
        if (curr.empty()) break;

        cvtColor(curr, curr_gray, COLOR_BGR2GRAY);
        calcOpticalFlowPyrLK(prev_gray, curr_gray, prev_pts, curr_pts, status, err);

        auto prev_it = prev_pts.begin();  // ������� �������� �����
        auto curr_it = curr_pts.begin();
        for (size_t k = 0; k < status.size(); k++)
        {
            if (status[k])
            {
                prev_it += 1;
                curr_it += 1;
            }
            else
            {
                prev_it = prev_pts.erase(prev_it);
                curr_it = curr_pts.erase(curr_it);
            }
        }

        Mat T = estimateRigidTransform(prev_pts, curr_pts, false); // ������� �-�� ��������������

        if (T.data == NULL) last_T.copyTo(T);
        T.copyTo(last_T);

        double dx = T.at<double>(0, 2);  // ��������� �� ��������� �������� � ��������
        double dy = T.at<double>(1, 2);
        double da = atan2(T.at<double>(1, 0), T.at<double>(0, 0));

        transforms.push_back(TransformParam(dx, dy, da));

        curr_gray.copyTo(prev_gray);

        for (int i = 0; i < curr_pts.size(); i += 1)  // ��������� ���. ������
        {
            line(imgLines, prev_pts[i], curr_pts[i], Scalar(0, 0, 255), 2);
            prev_pts[i].x = curr_pts[i].x;
            prev_pts[i].y = curr_pts[i].y;
        }
        curr += imgLines;
        of_out.write(curr);
    }
    int frame_radius = 70;
    vector <TransformParam> trajectory = countTrajectory(transforms);

    float SI1 = countSI(trajectory, 1);
    cout << "SI1" <<  " " << SI1 << endl;
    //for (int i = 0; i < trajectory.size(); i += 1)
    //{
    //    cout << trajectory[i].dx << " " << trajectory[i].dy << " " << trajectory[i].da << " " << endl;
    //}
    vector <TransformParam> smoothed_trajectory = smoothTrajectory(trajectory, frame_radius);
    float SI2 = countSI(smoothed_trajectory, 1);
    cout << "SI2" << " " << SI2 << endl;
    //for (int i = 0; i < smoothed_trajectory.size(); i += 1)
    //{
    //    cout << smoothed_trajectory[i].dx << " " << smoothed_trajectory[i].dy << " " << smoothed_trajectory[i].da << " " << endl;
    //}

    vector <TransformParam> transforms_smooth;

    for (int i = 0; i < transforms.size(); i += 1) // ��������������� ��������� ��������������
    {
        double diff_x = smoothed_trajectory[i].dx - trajectory[i].dx;
        double diff_y = smoothed_trajectory[i].dy - trajectory[i].dy;
        double diff_a = smoothed_trajectory[i].da - trajectory[i].da;

        double dx = transforms[i].dx + diff_x;
        double dy = transforms[i].dy + diff_y;
        double da = transforms[i].da + diff_a;

        transforms_smooth.push_back(TransformParam(dx, dy, da));
    }
    cap.set(CV_CAP_PROP_POS_FRAMES, 0);
    Mat T(2, 3, CV_64F);
    Mat frame, frame_stabilized, frame_out;

    //cap.release();
    //cout << n_frames << " " << transforms_smooth.size();
    for (int i = 0; i < n_frames - 2; i++)
    {
        cap.read(frame);
        if (frame.empty()) break;

        transforms_smooth[i].getTransform(T);
        warpAffine(frame, frame_stabilized, T, frame.size());

        out.write(frame_stabilized);

        //imshow("stabil", frame_stabilized);

        waitKey(10);
    }
    cout << endl;

    vector<float> PSNR2 = countPSNR("stabilized_bev_2.avi", n_frames);
    //for (int i = 0; i < PSNR2.size(); i += 1)
    //{
    //    cout << PSNR2[i] << " ";
    //}

    cap.release();
    out.release();
    destroyAllWindows();
    waitKey(10);

    return 0;
}